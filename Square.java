package bcas.ap.inter;

public class Square implements Shape {
    
	@Override
	public double getArea(double w, double h) {
		return w*h;
	}
	
	@Override
	public double getPerimeter(double w, double h) {
		return 4*w;
	}
	
	@Override
	public String getColor() {
		return "Green";
	} 

}
