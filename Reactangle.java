package bcas.ap.inter;

public class Reactangle implements Shape {
	
	@Override
	public double getArea(double w, double h) {
		return w*h;
	}
	
	@Override
	public double getPerimeter(double w, double h) {
		return 2*(w+h);
	}
	
	
	public String getColor() {
		return "Red";
	} 


}
